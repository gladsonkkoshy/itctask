import { useState } from "react";

const Messages = ({ selectedUser, users, setUsers }) => {
  const [messages, setMessages] = useState("");
  const [error, setError] = useState("");

  const onSubmit = () => {
    if (
      !messages ||
      messages.length === 0 ||
      messages === "" ||
      messages === " "
    ) {
      setError("Message is empty");
    } else {
      setError("");

      const index = users.findIndex((item) => item.id === selectedUser.id);

      const temp = users;
      temp[index].messages.push(messages);
      setUsers(temp);
      setMessages("");
    }
  };

  return (
    <div style={{ padding: 20 }}>
      <div style={{ display: "flex", alignItems: "center", padding: 10 }}>
        <div
          style={{
            backgroundColor: "green",
            width: 40,
            height: 40,
            borderRadius: "50%",
          }}
        ></div>
        <div style={{ paddingLeft: 20 }}>{selectedUser.name}</div>
      </div>
      <div style={{ display: "flex", flexDirection: "column" }}>
        {selectedUser.messages &&
          selectedUser.messages.map((message) => <p>{message}</p>)}
      </div>
      <div style={{ display: "flex", alignItems: "center" }}>
        <div
          style={{
            // backgroundColor: "yellow",
            display: "flex",
            flexDirection: "column",
          }}
        >
          <textarea
            name="messages"
            value={messages}
            onChange={(e) => setMessages(e.target.value)}
            style={{ borderColor: error && "red" }}
          />
          {error && <span style={{ color: "red" }}>{error}</span>}
        </div>
        <button onClick={onSubmit}>send</button>
      </div>
    </div>
  );
};

export default Messages;
