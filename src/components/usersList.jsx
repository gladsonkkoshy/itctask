const UsersList = ({ users, selectedUser, setSelectedUser }) => {
  console.log("users => ", users);
  return (
    <div
      style={{
        // backgroundColor: "yellow",
        padding: 20,
      }}
    >
      {users &&
        users.map((user) => (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              padding: 10,
              cursor: "pointer",
            }}
            onClick={(e) => setSelectedUser(user)}
          >
            <div
              style={{
                backgroundColor: "blue",
                width: 40,
                height: 40,
                borderRadius: "50%",
              }}
            ></div>
            <div
              style={{
                paddingLeft: 20,
                color: selectedUser.id === user.id ? "Blue" : "black",
              }}
              key={user.id}
            >
              {user.name}
            </div>
          </div>
        ))}
    </div>
  );
};

export default UsersList;
