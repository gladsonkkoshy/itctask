import { useState } from "react";
import "./App.css";
import UsersList from "./components/usersList";
import Messages from "./components/message";

const initalUsers = [
  {
    id: 1,
    name: "arun",
    messages: ["This is a user message", "Another message"],
  },
  { id: 2, name: "George", messages: ["This is a message", "Another message"] },
  {
    id: 3,
    name: "Job",
    messages: ["this is jobs message", "This is another Job messsage"],
  },
];

function App() {
  const [users, setUsers] = useState(initalUsers);
  const [selectedUser, setSelectedUser] = useState(initalUsers[0]);

  return (
    <>
      <div style={{ display: "flex" }}>
        <div
          style={{
            // backgroundColor: "red"
            height: "100vh",
            width: "30vw",
          }}
        >
          <UsersList
            users={users}
            selectedUser={selectedUser}
            setSelectedUser={setSelectedUser}
          />
        </div>
        <div
          style={{
            // backgroundColor: "blue",
            height: "100vh",
            width: "70vw",
          }}
        >
          <Messages
            selectedUser={selectedUser}
            users={users}
            setUsers={setUsers}
          />
        </div>
      </div>
    </>
  );
}

export default App;
